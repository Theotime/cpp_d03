/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:13 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 13:32:14 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __SCAVTRAP_HPP__
# define __SCAVTRAP_HPP__

#include "ClapTrap.hpp"
# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class ScavTrap : public ClapTrap {

	public:
		ScavTrap(ScavTrap const &obj);
		ScavTrap(const std::string name);
		~ScavTrap();
		void			challengeNewcomer(std::string const &target);

		ScavTrap		&operator=(ScavTrap const & obj);

	private :
		ScavTrap();
};

#endif
