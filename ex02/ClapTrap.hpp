/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:13 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 13:18:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __CLAPTRAP_HPP__
# define __CLAPTRAP_HPP__

# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class ClapTrap {

	public:
		ClapTrap(
			int hit_point,
			int max_hit_point,
			int energy_point,
			int max_energy_point,
			int level,
			int melee_attack_damage,
			int ranged_attack_damage,
			int armor_damage_reduction,
			const std::string name,
			const std::string start
		);
		ClapTrap(ClapTrap const & obj);
		~ClapTrap();

		int				getHitPoint()					const;
		int				getMaxHitPoint()				const;
		int				getEnergyPoint()				const;
		int				getMaxEnergyPoint()				const;
		int				getLevel()						const;
		int				getMeleeAttackDamage()			const;
		int				getRangedAttackDamage()			const;
		int				getArmorDamageReduction()		const;
		std::string		getName()						const;

		ClapTrap		*setHitPoint(int hit_point);
		ClapTrap		*setMaxHitPoint(int max_hit_point);
		ClapTrap		*setEnergyPoint(int energy_point);
		ClapTrap		*setMaxEnergyPoint(int max_energy_point);
		ClapTrap		*setLevel(int level);
		ClapTrap		*setMeleeAttackDamage(int melee_attack_damage);
		ClapTrap		*setRangedAttaclDamage(int ranged_attack_damage);
		ClapTrap		*setArmorDamageReduction(int armor_damage_reduction);
		ClapTrap		*setName(std::string name);

		void			rangedAttack(const std::string &target);
		void			meleeAttack(const std::string &target);
		void			info(std::string const action);

		int				takeDamage(unsigned int amount);
		int				beRepaired(unsigned int amount);

		ClapTrap		&operator=(ClapTrap const &obj);

	protected:
		int				_hit_point;
		int				_max_hit_point;
		int				_energy_point;
		int				_max_energy_point;
		int				_level;
		int				_melee_attack_damage;
		int				_ranged_attack_damage;
		int				_armor_damage_reduction;
		std::string		_name;
		ClapTrap();

	private:
		std::string		_start;

};

#endif
