/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:10 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 04:01:59 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int		main() {
	ScavTrap		scav("ScavTrap");
	FragTrap		frag("FragTrap");

	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(55);
	frag.info("Take damage 55");
	frag.beRepaired(30);
	frag.info("Repair 30");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(500);
	frag.info("Take damage 500");
	frag.beRepaired(300);
	frag.info("Repair 300");

	scav.challengeNewcomer("Pere noel");
	scav.takeDamage(55);
	scav.info("Take damage 55");
	scav.beRepaired(30);
	scav.info("Repair 30");
	scav.challengeNewcomer("Pere noel");
	scav.challengeNewcomer("Pere noel");
	scav.takeDamage(500);
	scav.info("Take damage 500");
	scav.beRepaired(300);
	scav.info("Repair 300");
	return (0);
}
