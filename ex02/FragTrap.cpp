/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:11 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 14:11:01 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"

FragTrap::FragTrap() : ClapTrap(100, 100, 100, 100, 1, 30, 20 , 5, "Default name FragTrap", "#"){
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}

FragTrap::FragTrap(FragTrap const &obj) : ClapTrap(obj) {
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}

FragTrap::FragTrap(const std::string name): ClapTrap(100, 100, 100, 100, 1, 30, 20 , 5, name, "#") {
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}

FragTrap::~FragTrap() {
	std::cout << "Destruct FragTrap : " << this->getName() << std::endl;
}

void		FragTrap::vaulthunter_dot_exe(std::string const & target) {
	static bool		init = false;
	int				type = 0;
	int				attacks = 0;
	std::string		pool[2][5] = {
		{
			"launched pony",
			"spat llama",
			"launched arraw",
			"DDOS",
			"Brut force"
		},				// Ranged
		{
			"tinky sock",
			"flicking Chuck Norris",
			"unicorn horn",
			"biffle",
			"Man in the middle"
		}  				// Melee
	};

	if (!init && (init = true))
		std::srand((time_t) this);
	if (this->getEnergyPoint() < 25) {
		std::cout << this->getName() << " does not have enough energy to perform this action." << std::endl;
		return ;
	}
	std::cout << target << " ";
	type = std::rand() % 2;
	attacks = std::rand() % 5;
	if (type != 1)
		this->rangedAttack(pool[type][attacks]);
	else
		this->meleeAttack(pool[type][attacks]);
	this->setEnergyPoint(this->getEnergyPoint() - 25);
}


/**
 * Operator
 **/
FragTrap		&FragTrap::operator=(FragTrap const &obj) {
	ClapTrap::operator=(obj);
	return (*this);
}
