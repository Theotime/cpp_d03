/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:10 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 04:25:39 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

int		main() {
	FragTrap		frag("FragTrap");

	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(55);
	frag.info("Take damage 55");
	frag.beRepaired(30);
	frag.info("Repair 30");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(500);
	frag.info("Take damage 500");
	frag.beRepaired(300);
	frag.info("Repair 300");

	return (0);
}
