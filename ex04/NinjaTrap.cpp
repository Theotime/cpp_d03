/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 06:32:17 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 17:49:41 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.hpp"
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

NinjaTrap::NinjaTrap() : ClapTrap(60, 60 , 120, 120, 1, 60, 5, 0, "Default name NinjaTrap", "*") {
	std::cout << "Create Ninja : " << this->getName()<< std::endl;
}

NinjaTrap::NinjaTrap(const std::string name): ClapTrap(60, 60 , 120, 120, 1, 60, 5, 0, name, "*") {
	std::cout << "Create Ninja : " << this->getName()<< std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const & obj) : ClapTrap(obj) {
	std::cout << "Create Ninja : " << this->getName()<< std::endl;
}

NinjaTrap::~NinjaTrap() {
	std::cout << "Destruct NinjaTrap : " << this->getName() << std::endl;
}

void			NinjaTrap::ninjaShoebox(std::string const &target) {
	static bool		init = false;
	std::string		pool[5] = {
		"Cactus",
		"Pocket",
		"Bouze",
		"Other",
		"Fuck"
	};

	if (this->getEnergyPoint() > 0) {
		if (!init && (init = true))
			std::srand((time_t) this);
		std::cout << this->getName() << " " << pool[std::rand() % 5] << " " << target << std::endl;
		this->setEnergyPoint(this->getEnergyPoint() - 25);
	} else {
		std::cout << this->getName() << " does not have enough energy to perform this action." << std::endl;
	}
}

void		NinjaTrap::ninjaShoebox(NinjaTrap const & obj) {
	this->ninjaShoebox(obj.getName());
}

void		NinjaTrap::ninjaShoebox(ClapTrap const & obj) {
	this->ninjaShoebox(obj.getName());	
}

void		NinjaTrap::ninjaShoebox(FragTrap const & obj) {
	this->ninjaShoebox(obj.getName());
}

void		NinjaTrap::ninjaShoebox(ScavTrap const & obj) {
	this->ninjaShoebox(obj.getName());
}

/**
 * Operator
 **/

NinjaTrap			&NinjaTrap::operator=(NinjaTrap const & obj) {
	ClapTrap::operator=(obj);
	return (*this);
}
