#ifndef __SUPERTRAP_HPP__
# define __SUPERTRAP_HPP__

# include "FragTrap.hpp"
# include "NinjaTrap.hpp"

class SuperTrap : public NinjaTrap, public FragTrap {

	public:
		SuperTrap(std::string const name);
		SuperTrap(SuperTrap const & obj);
		~SuperTrap();

		SuperTrap		&operator=(SuperTrap const & obj);

		void			rangedAttack(const std::string &target);
		void			meleeAttack(const std::string &target);

	private:
		SuperTrap();

};

#endif