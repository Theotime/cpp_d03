#include "ClapTrap.hpp"
#include "SuperTrap.hpp"
#include "FragTrap.hpp"
#include "NinjaTrap.hpp"


SuperTrap::SuperTrap() : ClapTrap(), NinjaTrap(), FragTrap() {
	std::cout << "Create SuperTrap : " << this->getName() << std::endl;
}

SuperTrap::~SuperTrap() {
	std::cout << "SuperTrap Destruct" << std::endl;
}

SuperTrap::SuperTrap(std::string const name) : ClapTrap(name), NinjaTrap(), FragTrap() {
	std::cout << "Create SuperTrap : " << this->getName() << std::endl;
}

SuperTrap::SuperTrap(SuperTrap const & obj) : ClapTrap(obj), NinjaTrap(obj), FragTrap(obj) {
	std::cout << "Create SuperTrap : " << this->getName() << std::endl;
}

void			SuperTrap::rangedAttack(const std::string &target) {
	FragTrap::rangedAttack(target);
}

void			SuperTrap::meleeAttack(const std::string &target) {
	NinjaTrap::rangedAttack(target);
}

SuperTrap		&SuperTrap::operator=(SuperTrap const & obj) {
	ClapTrap::operator=(obj);
	FragTrap::operator=(obj);
	NinjaTrap::operator=(obj);
	return (*this);
}