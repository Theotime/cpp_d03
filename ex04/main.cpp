/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:10 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 17:51:41 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

int		main() {
	ClapTrap		clap(100, 100, 100, 100, 100, 100, 100, 100, "ClapTrap", "!");
	ScavTrap		scav("ScavTrap");
	FragTrap		frag("FragTrap");
	NinjaTrap		ninja("NinjaTrap");
	NinjaTrap		ninja2("NinjaTrap 2");
	SuperTrap		super("SuperTrap");

	std::cout << std::endl;
	std::cout << "			### FRAG TRAP ###" << std::endl;
	frag.vaulthunter_dot_exe("Pere noel");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.vaulthunter_dot_exe("Pere noel");
	std::cout << std::endl;

	std::cout << "			### SCAV TRAP ###" << std::endl;
	scav.challengeNewcomer("Pere noel");
	scav.challengeNewcomer("Pere noel");
	scav.challengeNewcomer("Pere noel");
	std::cout << std::endl;

	std::cout << "			### NINJA TRAP ###" << std::endl;
	ninja.ninjaShoebox(clap);
	ninja.ninjaShoebox(scav);
	ninja.ninjaShoebox(frag);
	ninja.ninjaShoebox(ninja2);
	std::cout << std::endl;

	std::cout << "			### SUPER TRAP ###" << std::endl;
	super.vaulthunter_dot_exe("Pere noel");
	super.ninjaShoebox(clap);
	super.ninjaShoebox(scav);
	super.ninjaShoebox(frag);
	super.ninjaShoebox(ninja2);
	std::cout << std::endl;

	return (0);
}
