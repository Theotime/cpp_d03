/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:11 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 15:44:41 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"

ClapTrap::ClapTrap() {
	std::cout << "Un ClapTrap est contruit" << std::endl;
}
ClapTrap::ClapTrap(ClapTrap const &obj) {
	std::cout << "Un ClapTrap est contruit" << std::endl;
	*this = obj;
}
ClapTrap::~ClapTrap() {
	std::cout << "Un ClapTrap est détruit" << std::endl;
}

ClapTrap::ClapTrap(
	int hit_point,
	int max_hit_point,
	int energy_point,
	int max_energy_point,
	int level,
	int melee_attack_damage,
	int ranged_attack_damage,
	int armor_damage_reduction,
	const std::string name,
	const std::string start
) : 
	_hit_point(hit_point),
	_max_hit_point(max_hit_point),
	_energy_point(energy_point),
	_max_energy_point(max_energy_point),
	_level(level),
	_melee_attack_damage(melee_attack_damage),
	_ranged_attack_damage(ranged_attack_damage),
	_armor_damage_reduction(armor_damage_reduction),
	_name(name),
	_start(start)
{ 
	std::cout << "Un ClapTrap est contruit" << std::endl;
}


void		ClapTrap::rangedAttack(const std::string &target) {
	std::cout << "SC4V-TP " << this->getName() << " attacks " << target << " at range, causing " << this->getRangedAttackDamage() << std::endl;
}

void		ClapTrap::meleeAttack(const std::string &target) {
	std::cout << "SC4V-TP " << this->getName() << " attacks " << target << " at melee, causing " << this->getMeleeAttackDamage() << std::endl;
}

void		ClapTrap::info(std::string const action) {
	std::cout << this->_start << "###################################" 											<< std::endl;
	std::cout << this->_start << " Joueur  : " << this->getName()												<< std::endl;
	std::cout << this->_start << " Vie     : " << this->getHitPoint() << "/" << this->getMaxHitPoint()			<< std::endl;
	std::cout << this->_start << " Energy  : " << this->getEnergyPoint() << "/" << this->getMaxEnergyPoint()		<< std::endl;
	std::cout << this->_start << " Action  : " << action 														<< std::endl;
	std::cout << this->_start << "###################################" 											<< std::endl;
}

int			ClapTrap::beRepaired(unsigned int amount) {
	int	max_repair = this->getMaxHitPoint() - this->getHitPoint();

	this->setHitPoint(this->getHitPoint() + amount);
	if ((int)amount > max_repair)
		return (max_repair);
	return (amount);
}

int			ClapTrap::takeDamage(unsigned int amount) {
	int		degat = amount - this->getArmorDamageReduction();

	this->setHitPoint(this->getHitPoint() - degat);
	if (this->getHitPoint() > degat)
		return (amount);
	return (degat);
}

/**
 * GETTER
 **/
int				ClapTrap::getHitPoint()				const { return (this->_hit_point); 				}
int				ClapTrap::getMaxHitPoint()			const { return (this->_max_hit_point);			}
int				ClapTrap::getEnergyPoint()			const { return (this->_energy_point);			}
int				ClapTrap::getMaxEnergyPoint()		const { return (this->_max_energy_point);		}
int				ClapTrap::getLevel()				const { return (this->_level);					}
int				ClapTrap::getMeleeAttackDamage()	const { return (this->_melee_attack_damage);	}
int				ClapTrap::getRangedAttackDamage()	const { return (this->_ranged_attack_damage);	}
int				ClapTrap::getArmorDamageReduction()	const { return (this->_armor_damage_reduction);	}
std::string		ClapTrap::getName()					const { return (this->_name);					}

/**
 * SETTER
 **/

ClapTrap		*ClapTrap::setHitPoint(int hit_point) {
	if (hit_point > this->_max_hit_point)
		hit_point = this->_max_hit_point;
	else if (hit_point < 0)
		hit_point = 0;
	this->_hit_point = hit_point;
	return (this);
}

ClapTrap		*ClapTrap::setMaxHitPoint(int max_hit_point) {
	this->_max_hit_point = max_hit_point;
	return (this);
}

ClapTrap		*ClapTrap::setEnergyPoint(int energy_point) {
	if (energy_point > this->_max_energy_point)
		energy_point = this->_max_energy_point;
	else if (energy_point < 0)
		energy_point = 0;
	this->_energy_point = energy_point;
	return (this);
}

ClapTrap		*ClapTrap::setMaxEnergyPoint(int max_energy_point) {
	this->_max_energy_point = max_energy_point;
	return (this);
}

ClapTrap		*ClapTrap::setLevel(int level) {
	this->_level = level;
	return (this);
}

ClapTrap		*ClapTrap::setMeleeAttackDamage(int melee_attack_damage) {
	this->_melee_attack_damage = melee_attack_damage;
	return (this);
}

ClapTrap		*ClapTrap::setRangedAttaclDamage(int ranged_attack_damage) {
	this->_ranged_attack_damage = ranged_attack_damage;
	return (this);
}

ClapTrap		*ClapTrap::setArmorDamageReduction(int armor_damage_reduction) {
	this->_armor_damage_reduction = armor_damage_reduction;
	return (this);
}

ClapTrap		*ClapTrap::setName(std::string name) {
	this->_name = name;
	return (this);
}

/**
 * OPERATOR
 **/

ClapTrap		&ClapTrap::operator=(ClapTrap const &obj) {
	this->_hit_point = obj._hit_point;
	this->_max_hit_point = obj._max_hit_point;
	this->_energy_point = obj._energy_point;
	this->_max_energy_point = obj._max_energy_point;
	this->_level = obj._level;
	this->_melee_attack_damage = obj._melee_attack_damage;
	this->_ranged_attack_damage = obj._ranged_attack_damage;
	this->_armor_damage_reduction = obj._armor_damage_reduction;
	this->_name = obj._name;
	return (*this);
}
