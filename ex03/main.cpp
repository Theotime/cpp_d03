/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:10 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 17:51:41 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

int		main() {
	ClapTrap		clap(100, 100, 100, 100, 100, 100, 100, 100, "ClapTrap", "!");
	ScavTrap		scav("ScavTrap");
	FragTrap		frag("FragTrap");
	NinjaTrap		ninja("NinjaTrap");
	NinjaTrap		ninja2("NinjaTrap 2");

	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(55);
	frag.info("Take damage 55");
	frag.beRepaired(30);
	frag.info("Repair 30");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.vaulthunter_dot_exe("Pere noel");
	frag.takeDamage(500);
	frag.info("Take damage 500");
	frag.beRepaired(300);
	frag.info("Repair 300");

	scav.challengeNewcomer("Pere noel");
	scav.takeDamage(55);
	scav.info("Take damage 55");
	scav.beRepaired(30);
	scav.info("Repair 30");
	scav.challengeNewcomer("Pere noel");
	scav.challengeNewcomer("Pere noel");
	scav.takeDamage(500);
	scav.info("Take damage 500");
	scav.beRepaired(300);
	scav.info("Repair 300");

	ninja.ninjaShoebox(clap);
	ninja.ninjaShoebox(scav);
	ninja.takeDamage(55);
	ninja.info("Take damage 55");
	ninja.beRepaired(30);
	ninja.info("Repair 30");
	ninja.ninjaShoebox(frag);
	ninja.takeDamage(500);
	ninja.ninjaShoebox(ninja2);
	ninja.info("Take damage 500");
	ninja.beRepaired(300);
	ninja.info("Repair 300");
	return (0);
}
