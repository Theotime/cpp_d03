/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/09 07:54:13 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 16:32:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __NINJATRAP_CPP__
# define __NINJATRAP_CPP__

# include "ClapTrap.hpp"
# include "FragTrap.hpp"
# include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap {
	public:
		NinjaTrap(NinjaTrap const & obj);
		NinjaTrap(const std::string name);
		~NinjaTrap();

		void		ninjaShoebox(std::string const &target);
		void		ninjaShoebox(NinjaTrap const &obj);
		void		ninjaShoebox(ClapTrap const &obj);
		void		ninjaShoebox(FragTrap const &obj);
		void		ninjaShoebox(ScavTrap const &obj);

		NinjaTrap		&operator=(NinjaTrap const &obj);

	private:
		NinjaTrap();
};

#endif
