/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:11 by triviere          #+#    #+#             */
/*   Updated: 2015/01/09 04:27:16 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

ScavTrap::ScavTrap() : ClapTrap(100, 100, 50, 50, 1, 20, 15, 3, "Default name ScavTrap", "$") {
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
};

ScavTrap::ScavTrap(ScavTrap const &obj) : ClapTrap(obj) {
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
}

ScavTrap::ScavTrap(const std::string name): ClapTrap(100, 100, 50, 50, 1, 20, 15, 3, name, "$") {
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
}

ScavTrap::~ScavTrap() {
	std::cout << "Destruct ScavTrap : " << this->getName() << std::endl;
}

void		ScavTrap::challengeNewcomer(std::string const &target) {
	static bool		init;
	std::string		challenge[5] = {
		" launches challenge the launch of a sandwich that has dragged on a keyboard for a week ",
		" launches challenge to kill a pony under the eyes Thor has ",
		" launches challenge to eat hot pepper has 7 million has Zaz has ",
		" launches challenge to do 6 months of internship has Streamnation",
		" launches challenge to Zaz in the shower was "
	};

	if (this->getEnergyPoint() > 0) {
		if (!init && (init = true))
			std::srand((time_t) this);
		std::cout << this->getName() << challenge[std::rand() % 5] << target << std::endl;
		this->setEnergyPoint(this->getEnergyPoint() - 25);
	}
}


ScavTrap		&ScavTrap::operator=(ScavTrap const &obj) {
	return (ScavTrap&)ClapTrap::operator=(obj);
}