/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:13 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 13:18:12 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __ScavTrap_HPP__
# define __ScavTrap_HPP__

# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class ScavTrap {

	public:
		ScavTrap(const std::string name);
		ScavTrap(ScavTrap const &obj);
		~ScavTrap();

		int				getHitPoint()					const;
		int				getMaxHitPoint()				const;
		int				getEnergyPoint()				const;
		int				getMaxEnergyPoint()				const;
		int				getLevel()						const;
		int				getMeleeAttackDamage()			const;
		int				getRangedAttackDamage()			const;
		int				getArmorDamageReduction()		const;
		std::string		getName()						const;

		ScavTrap		*setHitPoint(int hit_point);
		ScavTrap		*setMaxHitPoint(int max_hit_point);
		ScavTrap		*setEnergyPoint(int energy_point);
		ScavTrap		*setMaxEnergyPoint(int max_energy_point);
		ScavTrap		*setLevel(int level);
		ScavTrap		*setMeleeAttackDamage(int melee_attack_damage);
		ScavTrap		*setRangedAttaclDamage(int ranged_attack_damage);
		ScavTrap		*setArmorDamageReduction(int armor_damage_reduction);
		ScavTrap		*setName(std::string name);

		void			rangedAttack(const std::string &target);
		void			meleeAttack(const std::string &target);
		void			challengeNewcomer(std::string const & target);
		void			info(std::string const action);

		int				takeDamage(unsigned int amount);
		int				beRepaired(unsigned int amount);

		ScavTrap		&operator=(ScavTrap const &obj);

	private:
		ScavTrap();

		int				_hit_point;
		int				_max_hit_point;
		int				_energy_point;
		int				_max_energy_point;
		int				_level;
		int				_melee_attack_damage;
		int				_ranged_attack_damage;
		int				_armor_damage_reduction;
		std::string		_name;

};

#endif
