/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:13 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 13:17:24 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FRAGTRAP_HPP__
# define __FRAGTRAP_HPP__

# include <string>
# include <iostream>
# include <ctime>
# include <cstdlib>

class FragTrap {

	public:
		FragTrap(const std::string name);
		FragTrap(FragTrap const &obj);
		~FragTrap();

		int				getHitPoint()					const;
		int				getMaxHitPoint()				const;
		int				getEnergyPoint()				const;
		int				getMaxEnergyPoint()				const;
		int				getLevel()						const;
		int				getMeleeAttackDamage()			const;
		int				getRangedAttackDamage()			const;
		int				getArmorDamageReduction()		const;
		std::string		getName()						const;

		FragTrap		*setHitPoint(int hit_point);
		FragTrap		*setMaxHitPoint(int max_hit_point);
		FragTrap		*setEnergyPoint(int energy_point);
		FragTrap		*setMaxEnergyPoint(int max_energy_point);
		FragTrap		*setLevel(int level);
		FragTrap		*setMeleeAttackDamage(int melee_attack_damage);
		FragTrap		*setRangedAttaclDamage(int ranged_attack_damage);
		FragTrap		*setArmorDamageReduction(int armor_damage_reduction);
		FragTrap		*setName(std::string name);

		void			rangedAttack(const std::string &target);
		void			meleeAttack(const std::string &target);
		void			vaulthunter_dot_exe(std::string const & target);
		void			info(std::string const action);

		int				takeDamage(unsigned int amount);
		int				beRepaired(unsigned int amount);

		FragTrap		&operator=(FragTrap const &obj);

	private:
		FragTrap();

		int				_hit_point;
		int				_max_hit_point;
		int				_energy_point;
		int				_max_energy_point;
		int				_level;
		int				_melee_attack_damage;
		int				_ranged_attack_damage;
		int				_armor_damage_reduction;
		std::string		_name;

};

#endif
