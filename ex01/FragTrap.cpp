/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:11 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 14:09:56 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.hpp"

FragTrap::FragTrap(const std::string name):
	_hit_point(100),
	_max_hit_point(100),
	_energy_point(100),
	_max_energy_point(100),
	_level(1),
	_melee_attack_damage(30),
	_ranged_attack_damage(20),
	_armor_damage_reduction(5),
	_name(name)
{
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}

FragTrap::FragTrap() {
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}
FragTrap::FragTrap(FragTrap const &obj) {
	*this = obj;
	std::cout << "Create FragTrap : " << this->getName() << std::endl;
}

FragTrap::~FragTrap() {
	std::cout << "Destruct FragTrap : " << this->getName() << std::endl;
}

void		FragTrap::rangedAttack(const std::string &target) {
	std::cout << "FR4G-TP " << this->getName() << " attacks " << target << " at range, causing " << this->getRangedAttackDamage() << std::endl;
}

void		FragTrap::meleeAttack(const std::string &target) {
	std::cout << "FR4G-TP " << this->getName() << " attacks " << target << " at melee, causing " << this->getMeleeAttackDamage() << std::endl;
}

void		FragTrap::info(std::string const action) {
	std::cout << "####################################" 										<< std::endl;
	std::cout << "# Joueur  : " << this->getName()												<< std::endl;
	std::cout << "# Vie     : " << this->getHitPoint() << "/" << this->getMaxHitPoint()			<< std::endl;
	std::cout << "# Energy  : " << this->getEnergyPoint() << "/" << this->getMaxEnergyPoint()	<< std::endl;
	std::cout << "# Action  : " << action 														<< std::endl;
	std::cout << "####################################" 										<< std::endl;
}

int		FragTrap::beRepaired(unsigned int amount) {
	int	max_repair = this->getMaxHitPoint() - this->getHitPoint();

	this->setHitPoint(this->getHitPoint() + amount);
	if ((int)amount > max_repair)
		return (max_repair);
	return (amount);
}

void		FragTrap::vaulthunter_dot_exe(std::string const & target) {
	static bool		init = false;
	int				type = 0;
	int				attacks = 0;
	std::string		pool[2][5] = {
		{"launched pony", "spat llama", "launched arraw", "DDOS", "Brut force"}, // Ranged
		{"tinky sock", "flicking Chuck Norris", "unicorn horn", "biffle", "Man in the middle"}  // Melee
	};

	if (!init && (init = true))
		std::srand((time_t) this);
	if (this->getEnergyPoint() < 25) {
		std::cout << this->getName() << " does not have enough energy to perform this action." << std::endl;
		return ;
	}
	
	type = std::rand() % 2;
	attacks = std::rand() % 5;
	this->info(pool[type][attacks]);
	if (type != 1)
		this->rangedAttack(target);
	else
		this->meleeAttack(target);
	this->setEnergyPoint(this->getEnergyPoint() - 25);
}

int			FragTrap::takeDamage(unsigned int amount) {
	int		degat = amount - this->getArmorDamageReduction();

	this->setHitPoint(this->getHitPoint() - degat);
	if (this->getHitPoint() > degat)
		return (amount);
	return (degat);
}

/**
 * GETTER
 **/
int				FragTrap::getHitPoint()				const { return (this->_hit_point); 				}
int				FragTrap::getMaxHitPoint()			const { return (this->_max_hit_point);			}
int				FragTrap::getEnergyPoint()			const { return (this->_energy_point);			}
int				FragTrap::getMaxEnergyPoint()		const { return (this->_max_energy_point);		}
int				FragTrap::getLevel()				const { return (this->_level);					}
int				FragTrap::getMeleeAttackDamage()	const { return (this->_melee_attack_damage);	}
int				FragTrap::getRangedAttackDamage()	const { return (this->_ranged_attack_damage);	}
int				FragTrap::getArmorDamageReduction()	const { return (this->_armor_damage_reduction);	}
std::string		FragTrap::getName()					const { return (this->_name);					}

/**
 * SETTER
 **/

FragTrap		*FragTrap::setHitPoint(int hit_point) {
	if (hit_point > this->_max_hit_point)
		hit_point = this->_max_hit_point;
	else if (hit_point < 0)
		hit_point = 0;
	this->_hit_point = hit_point;
	return (this);
}

FragTrap		*FragTrap::setMaxHitPoint(int max_hit_point) {
	this->_max_hit_point = max_hit_point;
	return (this);
}

FragTrap		*FragTrap::setEnergyPoint(int energy_point) {
	if (energy_point > this->_max_energy_point)
		energy_point = this->_max_energy_point;
	else if (energy_point < 0)
		energy_point = 0;
	this->_energy_point = energy_point;
	return (this);
}

FragTrap		*FragTrap::setMaxEnergyPoint(int max_energy_point) {
	this->_max_energy_point = max_energy_point;
	return (this);
}

FragTrap		*FragTrap::setLevel(int level) {
	this->_level = level;
	return (this);
}

FragTrap		*FragTrap::setMeleeAttackDamage(int melee_attack_damage) {
	this->_melee_attack_damage = melee_attack_damage;
	return (this);
}

FragTrap		*FragTrap::setRangedAttaclDamage(int ranged_attack_damage) {
	this->_ranged_attack_damage = ranged_attack_damage;
	return (this);
}

FragTrap		*FragTrap::setArmorDamageReduction(int armor_damage_reduction) {
	this->_armor_damage_reduction = armor_damage_reduction;
	return (this);
}

FragTrap		*FragTrap::setName(std::string name) {
	this->_name = name;
	return (this);
}

/**
 * OPERATOR
 **/

FragTrap		&FragTrap::operator=(FragTrap const &obj) {
	this->_hit_point = obj._hit_point;
	this->_max_hit_point = obj._max_hit_point;
	this->_energy_point = obj._energy_point;
	this->_max_energy_point = obj._max_energy_point;
	this->_level = obj._level;
	this->_melee_attack_damage = obj._melee_attack_damage;
	this->_ranged_attack_damage = obj._ranged_attack_damage;
	this->_armor_damage_reduction = obj._armor_damage_reduction;
	this->_name = obj._name;
	return (*this);
}
