/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/08 09:19:11 by triviere          #+#    #+#             */
/*   Updated: 2016/01/07 13:18:17 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.hpp"

ScavTrap::ScavTrap(const std::string name):
	_hit_point(100),
	_max_hit_point(100),
	_energy_point(50),
	_max_energy_point(50),
	_level(1),
	_melee_attack_damage(20),
	_ranged_attack_damage(15),
	_armor_damage_reduction(3),
	_name(name)
{
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
}

ScavTrap::ScavTrap() {
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
}
ScavTrap::ScavTrap(ScavTrap const &obj) {
	*this = obj;
	std::cout << "Create ScavTraps : " << this->getName() << std::endl;
}

ScavTrap::~ScavTrap() {
	std::cout << "Destruct ScavTrap : " << this->getName() << std::endl;
}

void		ScavTrap::rangedAttack(const std::string &target) {
	std::cout << "SC4V-TP " << this->getName() << " attacks " << target << " at range, causing " << this->getRangedAttackDamage() << std::endl;
}

void		ScavTrap::meleeAttack(const std::string &target) {
	std::cout << "SC4V-TP " << this->getName() << " attacks " << target << " at melee, causing " << this->getMeleeAttackDamage() << std::endl;
}

int			ScavTrap::beRepaired(unsigned int amount) {
	int	max_repair = this->getMaxHitPoint() - this->getHitPoint();

	this->setHitPoint(this->getHitPoint() + amount);
	if ((int)amount > max_repair)
		return (max_repair);
	return (amount);
}

void		ScavTrap::challengeNewcomer(std::string const &target) {
	static bool		init;
	std::string		challenge[5] = {
		" launches challenge the launch of a sandwich that has dragged on a keyboard for a week ",
		" launches challenge to kill a pony under the eyes Thor has ",
		" launches challenge to eat hot pepper has 7 million has Zaz has ",
		" launches challenge to do 6 months of internship has Streamnation",
		" launches challenge to Zaz in the shower was "
	};

	if (this->getEnergyPoint() > 0) {
		if (!init && (init = true))
			std::srand((time_t) this);
		std::cout << this->getName() << challenge[std::rand() % 5] << target << std::endl;
		this->setEnergyPoint(this->getEnergyPoint() - 25);
		this->info("challengeNewcomer");
	}
}

void		ScavTrap::info(std::string const action) {
	std::cout << "$###################################" 										<< std::endl;
	std::cout << "$ Joueur  : " << this->getName()												<< std::endl;
	std::cout << "$ Vie     : " << this->getHitPoint() << "/" << this->getMaxHitPoint()			<< std::endl;
	std::cout << "$ Energy  : " << this->getEnergyPoint() << "/" << this->getMaxEnergyPoint()	<< std::endl;
	std::cout << "$ Action  : " << action 														<< std::endl;
	std::cout << "$###################################" 										<< std::endl;
}

int			ScavTrap::takeDamage(unsigned int amount) {
	int		degat = amount - this->getArmorDamageReduction();

	this->setHitPoint(this->getHitPoint() - degat);
	if (this->getHitPoint() > degat)
		return (amount);
	return (degat);
}

/**
 * GETTER
 **/
int				ScavTrap::getHitPoint()				const { return (this->_hit_point); 				}
int				ScavTrap::getMaxHitPoint()			const { return (this->_max_hit_point);			}
int				ScavTrap::getEnergyPoint()			const { return (this->_energy_point);			}
int				ScavTrap::getMaxEnergyPoint()		const { return (this->_max_energy_point);		}
int				ScavTrap::getLevel()				const { return (this->_level);					}
int				ScavTrap::getMeleeAttackDamage()	const { return (this->_melee_attack_damage);	}
int				ScavTrap::getRangedAttackDamage()	const { return (this->_ranged_attack_damage);	}
int				ScavTrap::getArmorDamageReduction()	const { return (this->_armor_damage_reduction);	}
std::string		ScavTrap::getName()					const { return (this->_name);					}

/**
 * SETTER
 **/

ScavTrap		*ScavTrap::setHitPoint(int hit_point) {
	if (hit_point > this->_max_hit_point)
		hit_point = this->_max_hit_point;
	else if (hit_point < 0)
		hit_point = 0;
	this->_hit_point = hit_point;
	return (this);
}

ScavTrap		*ScavTrap::setMaxHitPoint(int max_hit_point) {
	this->_max_hit_point = max_hit_point;
	return (this);
}

ScavTrap		*ScavTrap::setEnergyPoint(int energy_point) {
	if (energy_point > this->_max_energy_point)
		energy_point = this->_max_energy_point;
	else if (energy_point < 0)
		energy_point = 0;
	this->_energy_point = energy_point;
	return (this);
}

ScavTrap		*ScavTrap::setMaxEnergyPoint(int max_energy_point) {
	this->_max_energy_point = max_energy_point;
	return (this);
}

ScavTrap		*ScavTrap::setLevel(int level) {
	this->_level = level;
	return (this);
}

ScavTrap		*ScavTrap::setMeleeAttackDamage(int melee_attack_damage) {
	this->_melee_attack_damage = melee_attack_damage;
	return (this);
}

ScavTrap		*ScavTrap::setRangedAttaclDamage(int ranged_attack_damage) {
	this->_ranged_attack_damage = ranged_attack_damage;
	return (this);
}

ScavTrap		*ScavTrap::setArmorDamageReduction(int armor_damage_reduction) {
	this->_armor_damage_reduction = armor_damage_reduction;
	return (this);
}

ScavTrap		*ScavTrap::setName(std::string name) {
	this->_name = name;
	return (this);
}

/**
 * OPERATOR
 **/

ScavTrap		&ScavTrap::operator=(ScavTrap const &obj) {
	this->_hit_point = obj._hit_point;
	this->_max_hit_point = obj._max_hit_point;
	this->_energy_point = obj._energy_point;
	this->_max_energy_point = obj._max_energy_point;
	this->_level = obj._level;
	this->_melee_attack_damage = obj._melee_attack_damage;
	this->_ranged_attack_damage = obj._ranged_attack_damage;
	this->_armor_damage_reduction = obj._armor_damage_reduction;
	this->_name = obj._name;
	return (*this);
}
